package src;

import java.util.ArrayList;

public abstract class Person
{
  Vitals m_vitals;
  String m_name;
  Boolean m_isAlive;
  ArrayList<Command> m_commandList;
  GameInstance m_gameInstance;
  
  int m_teamId;

  public Person(String n, Vitals v, int teamId)
  {
    m_name = n;
    m_vitals = new Vitals(v);
    m_teamId = teamId;
    m_isAlive = true;
    m_commandList = new ArrayList<Command>();
    m_gameInstance = GameInstance.getInstance();
  }
  
  // sends a command to a person - need a way to get that person from game instance (this will be in action selection phase)
  public void sendCommand(Command c, int targetId)
  {
    m_gameInstance.applyCommand(c, targetId);
  }
  
  // what do you think of this being a bool, returning "success" or not?
  // might not have a ton of use. could help with testing. I would assume we'd ignore the result of this most of the time though.
  public Boolean executeCommand()
  {
    if (!m_commandList.isEmpty())
    {
     
      Command toExecute = m_commandList.remove(0); // remove first item in list
      System.out.println(toExecute.toString() + " ---> " + m_name);
      toExecute.execute(this); // execute command on self; this line needs the word "execute" in it one more time. execute.
      return true;
    }
    else
    {
      //System.out.println("Nothing in the queue to execute.");
      return false;
    }
  }
  
  public void addCommand(Command c)
  {
    m_commandList.add(c);
  }
  
  public String toString()
  {
    return m_name + " " + m_vitals.toString();
  }
  
  
  public Vitals getVitals()
  {
    return m_vitals;
  }

  public void setVitals(Vitals vitals)
  {
    this.m_vitals = vitals;
  }

  public String getName()
  {
    return m_name;
  }

  public void setName(String name)
  {
    this.m_name = name;
  }

  public Boolean getIsAlive()
  {
    if (m_vitals.getHealth() > 0)
    {
      m_isAlive = true;
    }
    else
    {
      m_isAlive = false;
    }
    
    return m_isAlive;
  }

  public void setIsAlive(Boolean isAlive)
  {
    this.m_isAlive = isAlive;
  }

  public ArrayList<Command> getCommandList()
  {
    return m_commandList;
  }

  public void setCommandList(ArrayList<Command> commandList)
  {
    this.m_commandList = commandList;
  }
  
  public int getTeamId()
  {
    return m_teamId;
  }
  
  public void setTeamId(int m_teamId)
  {
    this.m_teamId = m_teamId;
  }
}
