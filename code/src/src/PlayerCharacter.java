package src;

import java.util.ArrayList;
import java.util.Scanner;

public class PlayerCharacter extends Person
{
  int m_level;
  int m_totalXp;
  
  public PlayerCharacter(String name, int teamId)
  {
    super(name, new Vitals(100, 100, 100), teamId);
    m_level = 1;
    m_totalXp = 0;
  }
  
  public PlayerCharacter(String name, Vitals v, int teamId)
  {
    super(name, v, teamId);
    m_level = 1;
    m_totalXp = 0;
  }
  
  public PlayerCharacter(String name, Vitals v, int level, int totalXp, int teamId)
  {
    super(name, v, teamId);
    m_level = level;
    m_totalXp = totalXp;
  }
  
  public Vitals getPositiveVitals(int num)
  {
	  int level = m_level + 1;
	  int health = num * level;
	  int stamina = num + (2*level);
	  int mana = num + (3*level);
	  return new Vitals(health, stamina, mana);
  }
  
  public Vitals getAttackVitals(int num)
  {
	  int level = m_level + 1;
	  int health = num * level;
	  int stamina = num + (2*level);
	  int mana = num + (3*level);
	  return new Vitals(health * -1, stamina *-1, mana*-1);
  }
  
  public Command pickCommand()
  {
	  Weapon Smiter = new Weapon("Smiter", getAttackVitals(1));
	  ArrayList<Command> commandsList = new ArrayList<Command>();
	  commandsList.add(new Attack("Smite of Smiting", getAttackVitals(3), Smiter));
	  commandsList.add(new Spell("FireBall", getAttackVitals(5)));
	  commandsList.add(new Potion("Healing Potion", getPositiveVitals(5)));
	  boolean inValidInput = true;
	  Scanner in = new Scanner(System.in);
	  int num = 0;
	  while(inValidInput)
	  {
		  System.out.println("Choose a command for " + m_name + ": ");
		  int index = 0;
		  for (Command c : commandsList)
		  {
			  System.out.println("\t" + index + ": " + c.toString());
			  index++;
		  }

		  String input = in.nextLine();
		  num = Integer.parseInt(input);
		  if (num >= 0 && num < commandsList.size())
		  {
			  inValidInput = false;
		  }
		  else
		  {
			  System.out.println("Invalid Input. Try Again Dillhole.");
		  }
	  }
	  //in.close();

	  return commandsList.get(num);
  }
  
  public int pickTarget()
  {
	  ArrayList<Person> personList = m_gameInstance.getPersonList().getPersonList();

	  boolean invalidInput = true;
	  Scanner in = new Scanner(System.in);
	  int num = 0;
	  while (invalidInput)
	  {
		  System.out.println("Choose someone to attack: ");
		  int index = 0;
		  for (Person p : personList)
		  {
			  System.out.println("\t" + index + ": " + p.toString());
			  index++;
		  }
		  System.out.println("");
		  String input = in.nextLine();
		  num = Integer.parseInt(input);
		  if(num >= 0 && num < personList.size())
		  {
			  invalidInput = false;
		  }
		  else
		  {
			  System.out.println("Invalid Input. Try again.");
		  }
	  }
	  //in.close();
	  return num;
  }
  
  public void checkIfLevelUp()
  {
    if (m_totalXp >= (m_level * 100))
    {
      levelUp();
    }
  }

  public void levelUp()
  {
    m_totalXp -= (m_level * 100);
    m_level++;
    m_vitals.Modify(new Vitals(10, 10, 10));
  }
  
  public int getLevel()
  {
    return m_level;
  }

  public void setLevel(int m_level)
  {
    this.m_level = m_level;
  }

  public int getTotalXp()
  {
    return m_totalXp;
  }

  public void setTotalXp(int m_totalXp)
  {
    this.m_totalXp = m_totalXp;
  }
  
  public void addXp(int xp)
  {
	  this.m_totalXp += xp;
  }
}
