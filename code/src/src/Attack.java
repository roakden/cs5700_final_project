package src;

public class Attack implements Command
{
	String m_Name;
	Vitals m_Vitals;
	Weapon m_Weapon;
	
	public Attack(String name, Vitals vitals, Weapon weapon)
	{
		m_Name = name;
		m_Vitals = vitals;
		m_Weapon = weapon;
	}
	
	public Attack(String name, Vitals vitals)
	{
		m_Name = name;
		m_Vitals = vitals;
		m_Weapon = new Weapon("no weapon", 
				new Vitals(0,0,0));
	}
	
	@Override
	public void execute(Person p)
	{
		//Set up temp vitals object for the attack
		Vitals AttackVitals = m_Vitals;
		//add the weapon vitals to the attack vitals
		AttackVitals.Modify(m_Weapon.getVitals());
		
		Vitals tempVitals = p.getVitals();
		tempVitals.Modify(AttackVitals);
		p.setVitals(tempVitals);
	}
	
	public String toString()
	{
		//Set up temp vitals object for the attack
		Vitals AttackVitals = m_Vitals;
		//add the weapon vitals to the attack vitals
		AttackVitals.Modify(m_Weapon.getVitals());
		
		return m_Name + " for " + AttackVitals.toString();
	}

	public String getName()
	{
		return m_Name;
	}

	public void setName(String m_Name)
	{
		this.m_Name = m_Name;
	}

	public Vitals getVitals()
	{
		return m_Vitals;
	}

	public void setVitals(Vitals m_Vitals)
	{
		this.m_Vitals = m_Vitals;
	}

	public Weapon getWeapon()
	{
		return m_Weapon;
	}

	public void setWeapon(Weapon m_Weapon)
	{
		this.m_Weapon = m_Weapon;
	}	
}
