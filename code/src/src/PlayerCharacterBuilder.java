package src;

import java.util.Scanner;

public class PlayerCharacterBuilder
{
  private String m_name;
  private Vitals m_vitals; 
  private int m_level; 
  private int m_totalXp;
  private int m_battlesWon;
  private int m_teamId;
  
  public PlayerCharacterBuilder()
  {
    // default values, if not specified during the building process
    m_name = "NoName";
    m_vitals = new Vitals(100, 100, 100);
    m_level = 1;
    m_totalXp = 0;
    m_battlesWon = 0; // for difficulty adjustments
    m_teamId = 0;
  }

  public PlayerCharacter createPlayerCharacter()
  {
    return new PlayerCharacter(m_name, m_vitals, m_level, m_totalXp, m_teamId);
  }
  
  public void characterCreation()
  {
    GameInstance g = GameInstance.getInstance();
    System.out.println("Welcome to an adventure filled with...stuff.");
    System.out.println("Begin character creation! (Coming soon in 1.0.1: Eyebrows)");
    Boolean continueCreatingCharacters = true;
    do
    {
      System.out.println("Do you want to create a default character or an advanced character? (Enter 'a' or 'd'): ");
      Scanner s = new Scanner(System.in);
      String input = s.nextLine();
      
      if (input.equals("d"))
      {
        System.out.println("Please enter your character's name:");
        String playerName = s.nextLine();
        this.setName(playerName);
        
        PlayerCharacter p = this.createPlayerCharacter();
        g.addPlayer(p);
      }
      else if (input.equals("a"))
      {
        System.out.println("Please enter your character's name:");
        String playerInput = s.nextLine();
        this.setName(playerInput);
        System.out.println("Enter character health value:");
        playerInput = s.nextLine();
        int health = Integer.parseInt(playerInput);
        System.out.println("Enter character stamina value:");
        playerInput = s.nextLine();
        int stamina = Integer.parseInt(playerInput);
        System.out.println("Enter character mana value:");
        playerInput = s.nextLine();
        int mana = Integer.parseInt(playerInput);
        
        Vitals vitals = new Vitals(health, stamina, mana);
        this.setVitals(vitals);
        
        System.out.println("Enter character level value:");
        playerInput = s.nextLine();
        int level = Integer.parseInt(playerInput);
        this.setLevel(level);

        System.out.println("Enter character xp value:");
        playerInput = s.nextLine();
        int xp = Integer.parseInt(playerInput);
        this.setTotalXp(xp);

        System.out.println("Enter character battlesWon value:");
        playerInput = s.nextLine();
        int battlesWon = Integer.parseInt(playerInput);
        this.setBattlesCompleted(battlesWon);
        
        PlayerCharacter p = this.createPlayerCharacter();
        g.addPlayer(p);
      }
      else
      {
        System.out.println("Invalid command. Character was not created.");
      }
      
      System.out.println("Do you want to create another character for your party?(y/n)");
      String question = s.nextLine();
      if (!question.equals("y"))
      {
        continueCreatingCharacters = false;
      }
      
    } while (continueCreatingCharacters);
  }
  
  public String getName()
  {
    return m_name;
  }

  public void setName(String name)
  {
    this.m_name = name;
  }

  public Vitals getVitals()
  {
    return m_vitals;
  }

  public void setVitals(Vitals vitals)
  {
    this.m_vitals = vitals;
  }

  public int getLevel()
  {
    return m_level;
  }

  public void setLevel(int level)
  {
    this.m_level = level;
  }

  public int getTotalXp()
  {
    return m_totalXp;
  }

  public void setTotalXp(int totalXp)
  {
    this.m_totalXp = totalXp;
  }

  public int getBattlesWon()
  {
    return m_battlesWon;
  }
  
  public void setTeamId(int teamId)
  {
    this.m_teamId = teamId;
  }

  public int getTeamId()
  {
    return m_teamId;
  }

  public void setBattlesCompleted(int battlesWon)
  {
    GameInstance g = GameInstance.getInstance();
    g.setBattlesCompleted(battlesWon);
  }
  
}
