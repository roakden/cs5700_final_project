package src;

public class EnemyBuilder
{
  private String m_name;
  private Vitals m_vitals;
  private int m_xpValue;
  
  public EnemyBuilder()
  {
    // default values
    m_name = "Enemy";
    m_vitals = new Vitals(50, 50, 50);
    m_xpValue = 10;
  }
  
  public Enemy createEnemy()
  {
    return new Enemy(m_name, m_vitals, m_xpValue, GameInstance.ENEMY_TEAM_ID);
  }
  
  public String getName()
  {
    return m_name;
  }
  
  public void setName(String name)
  {
    this.m_name = name;
  }
  
  public Vitals getVitals()
  {
    return m_vitals;
  }
  
  public void setVitals(Vitals vitals)
  {
    this.m_vitals = vitals;
  }
  
  public int getXpValue()
  {
    return m_xpValue;
  }
  
  public void setXpValue(int xpValue)
  {
    this.m_xpValue = xpValue;
  }
}
