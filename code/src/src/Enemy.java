package src;

import java.util.ArrayList;
import java.util.Random;

public class Enemy extends Person
{
  int m_xpValue;
  
  public Enemy(String name, Vitals v, int xp, int teamId)
  {
    super(name, v, teamId);
    m_xpValue = xp;
  }

  public void generateAction()
  {
    // can be overridden by subclasses
    // need to: select the move to do based on something (e.g. RNG)
    // select target
    // GameInstance g = GameInstance.getInstance();
    // Command toExecute = pickCommand();
    // int target = pickTarget();
    // g.applyCommand(toExecute, target);
  }
  
  public Command pickCommand()
  {
    ArrayList<Command> attacksList = new ArrayList<Command>();
    attacksList.add(new Attack("Punch", getRandomVitals()));
    attacksList.add(new Attack("ElectroZap", getRandomVitals()));
    attacksList.add(new Attack("SlamALlamaJamJam", getRandomVitals()));
    attacksList.add(new Attack("Hulk Smash", getRandomVitals()));
    
    int random = (int) (attacksList.size() * Math.random());
    Command c = attacksList.get(random);
    
    return c;
  }
  
  public int getRandomInt()
  {
	  int battles = m_gameInstance.getBattlesCompleted() + 1;
	  int battlesTwo = battles + 5;
	  Random rand = new Random();
	  int num = rand.nextInt((battlesTwo - battles) + 1) + battles;
	  return num;
  }
  
  public Vitals getRandomVitals()
  {
		int x = getRandomInt() * -1;
		int y = getRandomInt() * -1;
		int z = getRandomInt() * -1;
		Vitals vitals = new Vitals(x, y, z);		
		return vitals;
  }
  
  public int pickTarget()
  {
	  ArrayList<Person> List = m_gameInstance.getPersonList().getPersonList();
	  ArrayList<Person> targetList = new ArrayList<Person>();
	  
	  for (Person p : List)
	  {
		  if(p.getTeamId() == GameInstance.USER_TEAM_ID)
		  {
			  targetList.add(p);
		  }
	  }
	  
	  Random rand = new Random();
	  return rand.nextInt(targetList.size());
  }
  
  public int getXpValue()
  {
    return m_xpValue;
  }

  public void setXpValue(int xpValue)
  {
    this.m_xpValue = xpValue;
  }
}
