package src;

public class Potion implements Command
{
	private String m_Name;
	private Vitals m_Vitals;
	
	public Potion(String name, Vitals vitals)
	{
		m_Name = name;
		m_Vitals = vitals;
	}
	
	public String toString()
	{
		return m_Name + " for " + m_Vitals.toString();
	}
	
	@Override
	public void execute(Person p) 
	{
		Vitals temp = p.getVitals();
		temp.Modify(m_Vitals);
		p.setVitals(temp);
	}

	public String getName()
	{
		return m_Name;
	}

	public void setName(String m_Name)
	{
		this.m_Name = m_Name;
	}

	public Vitals getVitals()
	{
		return m_Vitals;
	}

	public void setVitals(Vitals m_Vitals)
	{
		this.m_Vitals = m_Vitals;
	}
}
