package src;

public class Vitals
{
  private int m_health;
  private int m_stamina;
  private int m_mana;
  
  public Vitals()
  {
    m_health = 0;
    m_stamina = 0;
    m_mana = 0;
  }
  
  public Vitals(int tempHealth, int tempStamina, int tempMana)
  {
    m_health = tempHealth;
    m_stamina = tempStamina;
    m_mana = tempMana;
  }
  
  public Vitals(Vitals v)
  {
    m_health = v.getHealth();
    m_stamina = v.getStamina();
    m_mana = v.getMana();
  }

  public void Modify(Vitals tempVitals)
  {
    m_health += tempVitals.getHealth();
    m_stamina += tempVitals.getStamina();
    m_mana += tempVitals.getMana();
  }
  
  public boolean Equals(Vitals tempVitals)
  {
    if (tempVitals.getHealth() == m_health && tempVitals.getStamina() == m_stamina
        && tempVitals.getMana() == m_mana)
    {
      return true;
    }
    else
    {
      return false;
    }
  }
  
  public void SetVitals(int newHealth, int newStamina, int newMana)
  {
    m_health = newHealth;
    m_stamina = newStamina;
    m_mana = newMana;
  }
  
  @Override
  public String toString()
  {
    return "H:" + m_health + ", S:" + m_stamina
        + ", M:" + m_mana;
  }

  public int getHealth()
  {
    return m_health;
  }
  
  public void setHealth(int m_health)
  {
    this.m_health = m_health;
  }
  
  public int getStamina()
  {
    return m_stamina;
  }
  
  public void setStamina(int m_stamina)
  {
    this.m_stamina = m_stamina;
  }
  
  public int getMana()
  {
    return m_mana;
  }
  
  public void setMana(int m_mana)
  {
    this.m_mana = m_mana;
  }  
}
