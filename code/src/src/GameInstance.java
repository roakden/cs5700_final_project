package src;

import java.util.ArrayList;
import java.util.Scanner;

public class GameInstance
{ 
  private static GameInstance m_instance;
  
  private PersonList m_personList;
  private int m_battlesCompleted;
  
  public static int USER_TEAM_ID = 0;
  public static int ENEMY_TEAM_ID = 1;
  
  private GameInstance()
  {
    m_personList = new PersonList();
    m_battlesCompleted = 0;
  }
  
  public static GameInstance getInstance()
  {
    if (m_instance == null)
    {
      m_instance = new GameInstance();
    }
    return m_instance;
  }
  
  
  public Person getPersonByIndex(int index)
  {
    return m_personList.getByIndex(index);
  }
  
  
  public void generateRandomEnemy()
  {
  	ArrayList<String> namesList = new ArrayList<String>();
  	namesList.add("Squidling");
  	namesList.add("Octopus");
  	namesList.add("Barbarian");
  	namesList.add("RoboKnight");
  	namesList.add("Amorphous Blob");
  	namesList.add("Karl the Krab");
  	namesList.add("Destructo the Destroyer");
  	
  	int nameIndex = (int) (namesList.size()*Math.random());
  	int health = (int) (10*Math.random());
  	health = health + 10;
  	int stamina = (int) (10*Math.random());
  	stamina = stamina + 10;
  	int mana = (int) (10*Math.random());
  	mana = mana + 10;
  	int xp = (int) (10*Math.random());
  	xp = xp + 10;
  	
  	//System.out.println("" + health + "" + stamina + "" + mana + "" + xp);
  	//System.out.println(m_battlesCompleted);
  	if(m_battlesCompleted > 1)
  	{
  		health = health * m_battlesCompleted;
  		stamina = stamina * m_battlesCompleted;
  		mana = mana * m_battlesCompleted;
  		xp = xp * m_battlesCompleted;
  	}
  	//System.out.println("" + health + "" + stamina + "" + mana + "" + xp);
  	
    this.m_personList.add(new Enemy(namesList.get(nameIndex), new Vitals(health, stamina, mana), xp, ENEMY_TEAM_ID)); // change charID to find what is available 
  }
  
  public void doCommands()
  {
    for (Person p: m_personList.getPersonList())
    {
      if (p.getTeamId() == ENEMY_TEAM_ID)
      {
        Enemy curEnemy = (Enemy)p;
        applyCommand(curEnemy.pickCommand(), curEnemy.pickTarget());
      }
      if (p.getTeamId() == USER_TEAM_ID)
      {
    	  PlayerCharacter curPlayer = (PlayerCharacter) p;
    	  Command pick = curPlayer.pickCommand();
    	  int pickT = curPlayer.pickTarget();
    	  applyCommand(pick, pickT);
      }
    }
  }
  
  public void run()
  {
	  PlayerCharacterBuilder build = new PlayerCharacterBuilder();
	  build.characterCreation();
	  Scanner in = new Scanner(System.in);
	  boolean keepGoing = true;
	  while (keepGoing)
	  {
		  runBattle();
		  if(m_personList.CheckIfDefeated(USER_TEAM_ID))
		  {
			  keepGoing = false;
			  System.out.println("Game Over!");
		  }
		  else
		  {
			  System.out.println("Do you want to keep playing (y/n): ");
			  String input = in.nextLine();
			  if (input.equals("n"))
			  {
				  keepGoing = false;
				  System.out.println("Thanks for Playing.");
				  in.close();
			  }
		  }		  
	  }
  }
  
  public void addPlayer(PlayerCharacter p)
  {
    m_personList.add(p);
  }
  
  public void applyCommand(Command c, int targetId)
  {
    Person target = getPersonByIndex(targetId);
    target.addCommand(c);
  }
  
  public void runBattle()
  {	  
    // generate 1-3 random enemies
	  int EnemyNum = (int) (3*Math.random());
	  EnemyNum += 1; 
	  
	  for(int i=0; i<EnemyNum; i++)
	  {
		  generateRandomEnemy();
	  }
	  
	  int totalBattleXp = 0;
	  for (Person p : m_personList.getPersonList())
	  {
		  if(p.getTeamId() == ENEMY_TEAM_ID)
		  {
			  Enemy pEnemy = (Enemy) p;
			  totalBattleXp += pEnemy.getXpValue();
		  }
	  }
	  
	  while(!m_personList.CheckIfDefeated(ENEMY_TEAM_ID) 
			  && !m_personList.CheckIfDefeated(USER_TEAM_ID))
	  {
		  runRound();
		  m_personList.CheckForDead();
	  }
	  
	  if(m_personList.CheckIfDefeated(USER_TEAM_ID))
	  {
		  System.out.println("You were defeated!");
	  }
	  
	  else
	  {
		  for(Person p : m_personList.getPersonList())
		  {
			  PlayerCharacter pChar = (PlayerCharacter) p;
			  pChar.addXp(totalBattleXp);
			  pChar.checkIfLevelUp();
		  }
	  }
	  
	  if(m_personList.CheckIfDefeated(ENEMY_TEAM_ID))
	  {
		  System.out.println("You defeated the enemy!");
		  m_personList.displayXp();
	  }
	  
    m_battlesCompleted++;
  }
  
  public void runRound()
  {
    // get player
    doCommands();
    // at the end execute lists
    m_personList.executeTurnCommands();
    System.out.println("");
    for (Person p : m_personList.getPersonList())
  	{
      	System.out.println("\t" + p.toString());
  	}
    System.out.println("");
  }

  public PersonList getPersonList()
  {
    return m_personList;
  }

  public void setPersonList(PersonList personList)
  {
    this.m_personList = personList;
  }

  public int getBattlesCompleted()
  {
    return m_battlesCompleted;
  }

  public void setBattlesCompleted(int battlesCompleted)
  {
    this.m_battlesCompleted = battlesCompleted;
  }
}
