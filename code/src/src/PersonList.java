package src;

import java.util.ArrayList;

public class PersonList
{
  private ArrayList<Person> m_personList;
  
  public PersonList()
  {
    m_personList = new ArrayList<Person>();
  }
  
  public PersonList(ArrayList<Person> newList)
  {
    m_personList = newList;
  }
  
  public void add(Person p)
  {
    // make a person with the specified team id and char id
    m_personList.add(p);
  }
  
  public void executeTurnCommands()
  {
    // do stuff
    for (Person personInGame : m_personList)
    {
      while(personInGame.executeCommand())
      {
        // keep executing
      }
    }
  }
  
  public Person getByIndex(int index)
  {
	  return m_personList.get(index);
  }
  
  public boolean CheckIfDefeated(int teamId)
  {
    boolean flag = true;
    for (Person person : m_personList)
    {
      if (person.getTeamId() == teamId)
      {
        flag = false;
      }
    }
    
    return flag;
  }
  
  public void CheckForDead()
  {
    ArrayList<Integer> indexes = new ArrayList<Integer>(); 
    
    for (Person person : m_personList)
    {
      if (!person.getIsAlive())
      {
        System.out.println("Player " + person.getName() + " died.  They have been removed from play");
        indexes.add(m_personList.indexOf(person));
      }
    }
    
    // Remove persons from the back to the front so you don't mess up the indexes
    for (int i = indexes.size() - 1; i >= 0; i--)
    {
      int indexToRemove = indexes.get(i);
      m_personList.remove(indexToRemove);
    }
  }

  public ArrayList<Person> getPersonList()
  {
    return m_personList;
  }

  public void setPersonList(ArrayList<Person> personList)
  {
    m_personList = personList;
  }
  
  public void displayXp()
  {
	  for(Person p : m_personList)
	  {
		  if (p.getTeamId() == GameInstance.USER_TEAM_ID)
		  {
			  PlayerCharacter pChar = (PlayerCharacter) p;
			  System.out.println(pChar.getName() + " has " + pChar.getTotalXp() + " xp and is level " + pChar.getLevel());
		  }
	  }
  }
}
