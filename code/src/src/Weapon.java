package src;

public class Weapon
{
	String m_Name;
	Vitals m_Vitals;
	
	public Weapon(String name, Vitals vitals) 
	{
		m_Name = name;
		m_Vitals = vitals;
	}

	public String getName()
	{
		return m_Name;
	}

	public void setName(String m_Name)
	{
		this.m_Name = m_Name;
	}

	public Vitals getVitals()
	{
		return m_Vitals;
	}

	public void setVitals(Vitals m_Vitals)
	{
		this.m_Vitals = m_Vitals;
	}	
}
