package src;

public interface Command
{
	public abstract void execute(Person p);
	public abstract String getName();
	public abstract Vitals getVitals();
	public abstract String toString();
}
