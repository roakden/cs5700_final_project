package test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import src.GameInstance;
import src.Person;
import src.PersonList;
import src.PlayerCharacter;
import src.Vitals;

public class testPersonList
{
  @Test
  public void PersonListDefaultConstructorTest()
  {
    PersonList personList = new PersonList();
    assertEquals(0, personList.getPersonList().size());
  }
  
  @Test
  public void PersonListConstructorTest()
  {
    ArrayList<Person> testList = new ArrayList<Person>();
    PlayerCharacter person = new PlayerCharacter("Noob", new Vitals(), GameInstance.USER_TEAM_ID);
    testList.add(person);
    PersonList personList = new PersonList(testList);
    assertEquals(1, personList.getPersonList().size());
    assertEquals(testList.get(0), personList.getPersonList().get(0));
  }
  
  @Test
  public void PersonListCheckIfDefeatedTest()
  {
    ArrayList<Person> testList = new ArrayList<Person>();
    PlayerCharacter person1 = new PlayerCharacter("Noob1", new Vitals(), GameInstance.USER_TEAM_ID);
    PlayerCharacter person2 = new PlayerCharacter("Noob2", new Vitals(), GameInstance.USER_TEAM_ID);
    PlayerCharacter person3 = new PlayerCharacter("Noob3", new Vitals(), GameInstance.USER_TEAM_ID);
    testList.add(person1);
    testList.add(person2);
    testList.add(person3);
    PersonList personList = new PersonList(testList);
    assertTrue(personList.CheckIfDefeated(GameInstance.ENEMY_TEAM_ID));
    
    PlayerCharacter person4 = new PlayerCharacter("Noob4", new Vitals(), GameInstance.ENEMY_TEAM_ID);
    testList.add(person4);

    personList.setPersonList(testList);
    assertFalse(personList.CheckIfDefeated(GameInstance.ENEMY_TEAM_ID));
  }
  
  @Test
  public void PersonListCheckForDeadTest()
  {
    ArrayList<Person> testList = new ArrayList<Person>();
    
    PlayerCharacter person1 = new PlayerCharacter("Noob1", new Vitals(0, 1, 1), GameInstance.USER_TEAM_ID);
    PlayerCharacter person2 = new PlayerCharacter("Noob2", new Vitals(1, 1, 1), GameInstance.USER_TEAM_ID);
    PlayerCharacter person3 = new PlayerCharacter("Noob3", new Vitals(0, 1, 1), GameInstance.USER_TEAM_ID);
    person1.getIsAlive();
    person2.getIsAlive();
    person3.getIsAlive();
    testList.add(person1);
    testList.add(person2);
    testList.add(person3);
    PersonList personList = new PersonList(testList);
    personList.CheckForDead();
    assertEquals(1, personList.getPersonList().size());
  }
  
  @Test
  public void PersonListCheckAddTest()
  {
    ArrayList<Person> testList = new ArrayList<Person>();
    
    PlayerCharacter person1 = new PlayerCharacter("Noob1", new Vitals(0, 1, 1), GameInstance.USER_TEAM_ID);
    PersonList personList = new PersonList(testList);
    personList.add(person1);
    assertEquals(1, personList.getPersonList().size());
  }
}
