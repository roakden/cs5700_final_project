package test;

import static org.junit.Assert.*;

import org.junit.Test;

import src.GameInstance;
import src.Person;
import src.PlayerCharacter;
import src.PlayerCharacterBuilder;
import src.Vitals;

public class testPlayerCharacterBuilder
{
  @Test
  public void testDefaultBuilder()
  {
    // test default build
    PlayerCharacterBuilder builder = new PlayerCharacterBuilder();
    PlayerCharacter p = builder.createPlayerCharacter();
    
    assertEquals("NoName H:100, S:100, M:100", p.toString());
  }
  
  @Test
  public void testSettingOneParam()
  {
    GameInstance g = GameInstance.getInstance();
    g.setBattlesCompleted(0); // make sure this is clear from other tests
    
    PlayerCharacterBuilder b1 = new PlayerCharacterBuilder();
    b1.setName("Frank");
    PlayerCharacter p1 = b1.createPlayerCharacter();
    assertEquals("Frank H:100, S:100, M:100", p1.toString());
    assertEquals(1, p1.getLevel());
    assertEquals(0, p1.getTotalXp());
    assertEquals(0, g.getBattlesCompleted());
    
    PlayerCharacterBuilder b2 = new PlayerCharacterBuilder();
    b2.setVitals(new Vitals(20, 50, 10));
    PlayerCharacter p2 = b2.createPlayerCharacter();
    assertEquals("NoName H:20, S:50, M:10", p2.toString());
    assertEquals(1, p2.getLevel());
    assertEquals(0, p2.getTotalXp());
    assertEquals(0, g.getBattlesCompleted());
    
    PlayerCharacterBuilder b3 = new PlayerCharacterBuilder();
    b3.setLevel(5);
    PlayerCharacter p3 = b3.createPlayerCharacter();
    assertEquals("NoName H:100, S:100, M:100", p3.toString());
    assertEquals(5, p3.getLevel());
    assertEquals(0, p3.getTotalXp());
    assertEquals(0, g.getBattlesCompleted());
    
    PlayerCharacterBuilder b4 = new PlayerCharacterBuilder();
    b4.setTotalXp(50);
    PlayerCharacter p4 = b4.createPlayerCharacter();
    assertEquals("NoName H:100, S:100, M:100", p3.toString());
    assertEquals(1, p4.getLevel());
    assertEquals(50, p4.getTotalXp());
    assertEquals(0, g.getBattlesCompleted());
    
    PlayerCharacterBuilder b5 = new PlayerCharacterBuilder();
    b5.setBattlesCompleted(12);
    PlayerCharacter p5 = b5.createPlayerCharacter();
    assertEquals("NoName H:100, S:100, M:100", p5.toString());
    assertEquals(1, p5.getLevel());
    assertEquals(0, p5.getTotalXp());
    assertEquals(12, g.getBattlesCompleted());
  }
  
  @Test
  public void testSettingAll()
  {
    GameInstance g = GameInstance.getInstance();
    g.setBattlesCompleted(0); // make sure this is clear from other tests
    
    PlayerCharacterBuilder b1 = new PlayerCharacterBuilder();
    b1.setName("Frank");
    b1.setVitals(new Vitals(20, 30, 40));
    b1.setLevel(3);
    b1.setTotalXp(31);
    b1.setTeamId(0);
    b1.setBattlesCompleted(6);
    PlayerCharacter p1 = b1.createPlayerCharacter();
    assertEquals("Frank H:20, S:30, M:40", p1.toString());
    assertEquals(3, p1.getLevel());
    assertEquals(31, p1.getTotalXp());
    assertEquals(6, g.getBattlesCompleted());
  }
  
//  @Test
//  public void testCharacterCreationProcess()
//  {
//    GameInstance g = GameInstance.getInstance();
//    PlayerCharacterBuilder b = new PlayerCharacterBuilder();
//    b.characterCreation();
//    for (Person p : g.getPersonList().getPersonList())
//    {
//      System.out.println(p.toString());
//    }
//  }
}
