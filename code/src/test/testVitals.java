package test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import src.Vitals;

public class testVitals
{
  @Test
  public void VitalsDefaultConstructorTest()
  {
    Vitals vitals = new Vitals();    
    assertEquals(0, vitals.getHealth());
    assertEquals(0, vitals.getStamina());
    assertEquals(0, vitals.getMana());
  }
  
  @Test
  public void VitalsConstructor1Test()
  {
    Vitals vitals = new Vitals(5, 6, 7);    
    assertEquals(5, vitals.getHealth());
    assertEquals(6, vitals.getStamina());
    assertEquals(7, vitals.getMana());
  }
  
  @Test
  public void VitalsEqualTest()
  {
    Vitals vitals1 = new Vitals(5, 6, 7);
    Vitals vitals2 = new Vitals(5, 6, 7);     
    assertEquals(true, vitals1.Equals(vitals2));
    assertEquals(true, vitals2.Equals(vitals1));
    
    Vitals vitals3 = new Vitals();
    Vitals vitals4 = new Vitals(1, 2, 3);     
    assertEquals(false, vitals3.Equals(vitals4));
    assertEquals(false, vitals4.Equals(vitals3));
  }
  
  @Test
  public void VitalsModifyTest()
  {
    Vitals vitals1 = new Vitals(5, 6, 7); 
    Vitals vitals2 = new Vitals(1, 1, 1);
    vitals1.Modify(vitals2);
    assertEquals(6, vitals1.getHealth());
    assertEquals(7, vitals1.getStamina());
    assertEquals(8, vitals1.getMana());   
    
    Vitals vitals3 = new Vitals(5, 6, 7); 
    Vitals vitals4 = new Vitals(-1, -1, -1);
    vitals3.Modify(vitals4);
    assertEquals(4, vitals3.getHealth());
    assertEquals(5, vitals3.getStamina());
    assertEquals(6, vitals3.getMana()); 
  }
  
  @Test
  public void VitalsSetVitalsTest()
  {
    Vitals vitals = new Vitals();    
    assertEquals(0, vitals.getHealth());
    assertEquals(0, vitals.getStamina());
    assertEquals(0, vitals.getMana());
    
    vitals.SetVitals(2, 2, 2);
    assertEquals(2, vitals.getHealth());
    assertEquals(2, vitals.getStamina());
    assertEquals(2, vitals.getMana());
  }
  
  @Test
  public void VitalsToStringTest()
  {
    Vitals vitals = new Vitals();    
    assertEquals(0, vitals.getHealth());
    assertEquals(0, vitals.getStamina());
    assertEquals(0, vitals.getMana());
    
    assertEquals("H:0, S:0, M:0", vitals.toString());
  }
}
