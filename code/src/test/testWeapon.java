package test;

import static org.junit.Assert.*;

import org.junit.Test;

import src.Vitals;
import src.Weapon;

public class testWeapon
{

	@Test
	public void testWeaponConstructor()
	{
		String name = "Awesome Weapon";
		Vitals vitals = new Vitals(10, 10, 10);
		Weapon testWeapon = new Weapon(name, vitals);
		
		assertEquals(name, testWeapon.getName());
		assertTrue(vitals.Equals(testWeapon.getVitals()));
	}

}
