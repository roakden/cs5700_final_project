package test;

import org.junit.Test;

import src.GameInstance;
import src.PlayerCharacter;
import src.Vitals;
import static org.junit.Assert.*;


public class testCharacter
{
  @Test
  public void createCharacter()
  {
    PlayerCharacter c = new PlayerCharacter("JimBobJoeJimothy", GameInstance.USER_TEAM_ID);
    
    assertEquals("JimBobJoeJimothy", c.getName());
    assertEquals(100, c.getVitals().getHealth());
    assertEquals(100, c.getVitals().getStamina());
    assertEquals(100, c.getVitals().getMana());
    assertEquals(1, c.getLevel());
    assertEquals(0, c.getTotalXp());
    assertEquals(0, c.getTeamId());
    assertTrue(c.getIsAlive());
    
    c = new PlayerCharacter("JimBobJoeJimothy", new Vitals(20, 30, 40), GameInstance.USER_TEAM_ID);
    
    assertEquals("JimBobJoeJimothy", c.getName());
    assertEquals(20, c.getVitals().getHealth());
    assertEquals(30, c.getVitals().getStamina());
    assertEquals(40, c.getVitals().getMana());
    assertEquals(1, c.getLevel());
    assertEquals(0, c.getTotalXp());
    assertEquals(0, c.getTeamId());
    assertTrue(c.getIsAlive());
    
    c = new PlayerCharacter("JimBobJoeJimothy", new Vitals(50, 20, 100), 4, 87, GameInstance.USER_TEAM_ID);
    
    assertEquals("JimBobJoeJimothy", c.getName());
    assertEquals(50, c.getVitals().getHealth());
    assertEquals(20, c.getVitals().getStamina());
    assertEquals(100, c.getVitals().getMana());
    assertEquals(4, c.getLevel());
    assertEquals(87, c.getTotalXp());
    assertEquals(0, c.getTeamId());
    assertTrue(c.getIsAlive());
    
    c = new PlayerCharacter("HeDead", new Vitals(0, 20, 30), GameInstance.USER_TEAM_ID);
    
    assertEquals("HeDead", c.getName());
    assertEquals(0, c.getVitals().getHealth());
    assertEquals(20, c.getVitals().getStamina());
    assertEquals(30, c.getVitals().getMana());
    assertEquals(1, c.getLevel());
    assertEquals(0, c.getTotalXp());
    assertEquals(0, c.getTeamId());
    assertFalse(c.getIsAlive());
  }
  
  @Test
  public void stringOutput()
  {
    PlayerCharacter c = new PlayerCharacter("JimBobJoeJimothy", GameInstance.USER_TEAM_ID); 
    assertEquals("JimBobJoeJimothy H:100, S:100, M:100", c.toString());
  }
  
  @Test
  public void levelUpTest()
  {
    PlayerCharacter c = new PlayerCharacter("JimBobJoeJimothy", GameInstance.USER_TEAM_ID); 
    assertEquals("JimBobJoeJimothy H:100, S:100, M:100", c.toString());
    c.setLevel(1);
    c.setTotalXp(100);
    c.checkIfLevelUp();
    assertEquals(0, c.getTotalXp());
    assertEquals(2, c.getLevel());
    c.setTotalXp(150);
    c.checkIfLevelUp();
    assertEquals(150, c.getTotalXp());
    assertEquals(2, c.getLevel());
    c.setTotalXp(210);
    c.checkIfLevelUp();
    assertEquals(10, c.getTotalXp());
    assertEquals(3, c.getLevel());
    assertEquals("JimBobJoeJimothy H:120, S:120, M:120", c.toString());
  }
  
  @Test
  public void TestAddXp()
  {
	  PlayerCharacter c = new PlayerCharacter("JimBobJoeJimothy", GameInstance.USER_TEAM_ID);  
	  c.addXp(15);
	  assertEquals(c.getTotalXp(), 15);
  }
}

