package test;

import static org.junit.Assert.*;

import org.junit.Test;

import src.Attack;
import src.Command;
import src.Enemy;
import src.GameInstance;
import src.Person;
import src.PlayerCharacter;
import src.Vitals;
import src.Weapon;

public class testAttack
{

	String name = "test name";
	Vitals vitals = new Vitals(-10, -10, -10);
	Vitals finalVitals = new Vitals(10,10,10);
	Vitals PlayerVitals = new Vitals(30,30,30);
	Weapon weapon = new Weapon("test weapon", vitals);
	Command testAttack = new Attack(name, vitals, weapon);
	Person testPlayer = new PlayerCharacter("name", PlayerVitals, GameInstance.USER_TEAM_ID);
	Person testEnemy = new Enemy("name", PlayerVitals, 1, GameInstance.ENEMY_TEAM_ID);
	
	@Test
	public void testAttackConstructor()
	{
		assertEquals(name, testAttack.getName());
		assertTrue(vitals.Equals(testAttack.getVitals()));
		assertEquals(weapon.getName(), "test weapon");
		assertTrue(vitals.Equals(weapon.getVitals()));
	}
	
	@Test
	public void testAttackExecutePlayerCharacter()
	{
		testAttack.execute(testPlayer);
		assertTrue(finalVitals.Equals(testPlayer.getVitals()));
	}
	
	@Test
	public void testAttackExecuteEnemy()
	{
		testAttack.execute(testEnemy);
		assertTrue(finalVitals.Equals(testEnemy.getVitals()));
	}
	
	@Test
	public void testAttackNoWeapon()
	{
		testEnemy = new Enemy("name", PlayerVitals, 1, GameInstance.ENEMY_TEAM_ID);
		Command tempAttack = new Attack("name", new Vitals(-10,-10,-10));
		Vitals testVitals = new Vitals(20,20,20);
		tempAttack.execute(testEnemy);
		assertTrue(testVitals.Equals(testEnemy.getVitals()));
	}
	
	@Test
	public void testAttackToString()
	{
		Command tempAttack = new Attack("name", new Vitals(-10,-10,-10));
		String test = "name for H:-10, S:-10, M:-10";
		//System.out.println(tempAttack.toString());
		assertEquals(test, tempAttack.toString());
	}

}
