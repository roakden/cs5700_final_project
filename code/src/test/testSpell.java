package test;

import static org.junit.Assert.*;

import org.junit.Test;

import src.Command;
import src.Enemy;
import src.GameInstance;
import src.Person;
import src.PlayerCharacter;
import src.Spell;
import src.Vitals;

public class testSpell
{
	String name = "test name";
	Vitals vitals = new Vitals(10, 10, 10);
	Vitals playerVitals = new Vitals(10,10,10);
	Vitals finalVitals = new Vitals(20,20,20);
	Command testSpell = new Spell(name, vitals);
	Person testPlayer = new PlayerCharacter("name", playerVitals, GameInstance.USER_TEAM_ID);
	Person testEnemy = new Enemy("name", playerVitals, 1, GameInstance.ENEMY_TEAM_ID);
	
	@Test
	public void testSpellConstructor()
	{
		assertEquals(name, testSpell.getName());
		assertTrue(vitals.Equals(testSpell.getVitals()));
	}
	
	@Test
	public void testSpellExecute()
	{
		testSpell.execute(testPlayer);
		assertTrue(finalVitals.Equals(testPlayer.getVitals()));
	}
	
	@Test
	public void testSpellExecuteEnemy()
	{
		testSpell.execute(testEnemy);
		assertTrue(finalVitals.Equals(testEnemy.getVitals()));
		
	}
	
	@Test
	public void testToString()
	{
		//System.out.println(testSpell.toString());
		String test = "test name for H:10, S:10, M:10";
		assertEquals(test, testSpell.toString());
	}
}
