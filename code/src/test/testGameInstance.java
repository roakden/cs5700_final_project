package test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import src.GameInstance;
import src.Person;
import src.PersonList;
import src.PlayerCharacter;
import src.Vitals;

public class testGameInstance
{
  @Test
  public void TestConstructor()
  {
    ArrayList<Person> personsInGameList = new ArrayList<Person>();
    PersonList personList = new PersonList();
    PlayerCharacter person1 = new PlayerCharacter("Noob1", new Vitals(), GameInstance.USER_TEAM_ID);
    personsInGameList.add(person1);
    
    personList.setPersonList(personsInGameList);
    GameInstance game = GameInstance.getInstance();
    game.setPersonList(personList);
    assertEquals(1, game.getPersonList().getPersonList().size());
  }
  
  @Test
  public void TestGetRandomEnemy()
  {
//    ArrayList<PersonInGame> personsInGameList = new ArrayList<PersonInGame>();
//    PersonList personList = new PersonList();
//    PersonInGame person1 = new PersonInGame(new PlayerCharacter("Noob1", new Vitals()), 0, 1);
//    PersonInGame person2 = new PersonInGame(new PlayerCharacter("Noob2", new Vitals()), 0, 2);
//    PersonInGame person3 = new PersonInGame(new PlayerCharacter("Noob3", new Vitals()), 0, 3);
//    personsInGameList.add(person1);
//    personsInGameList.add(person2);
//    personsInGameList.add(person3);
//    
//    personList.setPersonsInGame(personsInGameList);
//    GameInstance game = GameInstance.getInstance();
//    game.setPersonList(personList);
//    PersonInGame returned = game.GenerateRandomEnemy();
//    System.out.println(returned.get_person().getName() + " Returned");
//    assertEquals(0, returned.get_teamId());
  }
  
  @Test
  public void TestRandomEnemyGeneratorNoBattlesWon()
  {
	  GameInstance game = GameInstance.getInstance();
	  game.setBattlesCompleted(0);
	  game.getPersonList().getPersonList().clear();
	  game.generateRandomEnemy();
	  game.generateRandomEnemy();
	  game.generateRandomEnemy();
	  	  
	  PersonList list = game.getPersonList();
	  for(Person p : list.getPersonList())
	  {
		  System.out.println(p.toString());
		  Vitals pVitals = p.getVitals();
		  assertTrue(pVitals.getHealth() >=10 && pVitals.getHealth() < 20);
		  assertTrue(pVitals.getStamina() >=10 && pVitals.getStamina() < 20);
		  assertTrue(pVitals.getMana() >=10 && pVitals.getMana() < 20);
	  }
	  
	  
  }
  
  @Test
  public void TestRandomEnemyGeneratorTwo()
  {
	  GameInstance game = GameInstance.getInstance();
	  game.getPersonList().getPersonList().clear();
	  game.setBattlesCompleted(2);
	  game.generateRandomEnemy();
	  game.generateRandomEnemy();	  
	  PersonList list = game.getPersonList();
	  for(Person p : list.getPersonList())
	  {
		  System.out.println(p.toString());
		  Vitals pVitals = p.getVitals();
		  assertTrue(pVitals.getHealth() >=20 && pVitals.getHealth() < 40);
		  assertTrue(pVitals.getStamina() >=20 && pVitals.getStamina() < 40);
		  assertTrue(pVitals.getMana() >=20 && pVitals.getMana() < 40);
	  }
	  
	  
  }
  
  @Test
  public void TestRandomEnemyGeneratorFive()
  {
	  GameInstance game = GameInstance.getInstance();
	  game.getPersonList().getPersonList().clear();
	  game.setBattlesCompleted(5);
	  game.generateRandomEnemy();
	  game.generateRandomEnemy();	  
	  PersonList list = game.getPersonList();
	  for(Person p : list.getPersonList())
	  {
		  System.out.println(p.toString());
		  Vitals pVitals = p.getVitals();
		  assertTrue(pVitals.getHealth() >=50 && pVitals.getHealth() < 100);
		  assertTrue(pVitals.getStamina() >=50 && pVitals.getStamina() < 100);
		  assertTrue(pVitals.getMana() >=50 && pVitals.getMana() < 100);
	  }
	  
	  
  }
  
}
