package test;

import static org.junit.Assert.*;

import org.junit.Test;

import src.Command;
import src.Enemy;
import src.GameInstance;
import src.Person;
import src.PlayerCharacter;
import src.Potion;
import src.Vitals;

public class testPotion
{
	String name = "test name";
	Vitals vitals = new Vitals(10, 10, 10);
	Command testPotion = new Potion(name, vitals);
	Vitals playerVitals = new Vitals(10,10,10);
	Vitals finalVitals = new Vitals(20,20,20);
	Person testPlayer = new PlayerCharacter("name", playerVitals, GameInstance.USER_TEAM_ID);
	Person testEnemy = new Enemy("name", playerVitals, 1, GameInstance.ENEMY_TEAM_ID);
	
	@Test
	public void testPotionConstructor()
	{
		assertEquals(name, testPotion.getName());
		assertTrue(vitals.Equals(testPotion.getVitals()));
	}
	
	@Test
	public void testPotionExecutePlayerCharacter()
	{
		testPotion.execute(testPlayer);
		assertTrue(finalVitals.Equals(testPlayer.getVitals()));
	}
	
	@Test
	public void testPotionExecuteEnemy()
	{
		testPotion.execute(testEnemy);
		assertTrue(finalVitals.Equals(testEnemy.getVitals()));
	}
	
	@Test
	public void testToString()
	{
		//System.out.println(testPotion.toString());
		String test = "test name for H:10, S:10, M:10";
		assertEquals(test, testPotion.toString());
	}

}
