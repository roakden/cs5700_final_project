package test;

import static org.junit.Assert.*;

import org.junit.Test;

import src.Command;
import src.Enemy;
import src.GameInstance;
import src.Vitals;

public class testEnemy
{
  @Test
  public void constructor()
  {
    Enemy e = new Enemy("a", new Vitals(100, 20, 30), 10, GameInstance.ENEMY_TEAM_ID);
    assertEquals("a", e.getName());
    assertEquals(100, e.getVitals().getHealth());
    assertEquals(20, e.getVitals().getStamina());
    assertEquals(30, e.getVitals().getMana());
    assertEquals(10, e.getXpValue());
    assertEquals(1, e.getTeamId());
    assertTrue(e.getIsAlive());
  }
  
  @Test
  public void randomtest()
  {
    GameInstance g = GameInstance.getInstance();
    Enemy e = new Enemy("e", new Vitals(10,10,10), 10, GameInstance.ENEMY_TEAM_ID);
    g.setBattlesCompleted(0);
    int result;
    for (int i=0; i < 10; ++i)
    {
      result = e.getRandomInt();
      assertTrue(result >= 1 && result <= 6);
    }
  }
  
  @Test
  public void pickCommand()
  {
    Enemy e = new Enemy("e", new Vitals(10,10,10), 10, GameInstance.ENEMY_TEAM_ID);
    Command c;
    
    for (int i = 0; i < 10; ++i)
    {
      c = e.pickCommand();
      System.out.println(c.toString());
    }
  }
}
