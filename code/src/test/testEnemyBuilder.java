package test;

import static org.junit.Assert.*;

import org.junit.Test;

import src.Enemy;
import src.EnemyBuilder;
import src.Vitals;

public class testEnemyBuilder
{
  @Test
  public void testDefault()
  {
    EnemyBuilder b1 = new EnemyBuilder();
    Enemy e1 = b1.createEnemy();
    
    assertEquals("Enemy H:50, S:50, M:50",  e1.toString());
    assertEquals(10, e1.getXpValue());
  }
  
  @Test
  public void testOneParamSet()
  {
    EnemyBuilder b1 = new EnemyBuilder();
    b1.setName("Octopus");
    Enemy e1 = b1.createEnemy();
    assertEquals("Octopus H:50, S:50, M:50",  e1.toString());
    assertEquals(10, e1.getXpValue());
    
    EnemyBuilder b2 = new EnemyBuilder();
    b2.setVitals(new Vitals(1000, 200, 300));
    Enemy e2 = b2.createEnemy();
    assertEquals("Enemy H:1000, S:200, M:300",  e2.toString());
    assertEquals(10, e2.getXpValue());
    
    EnemyBuilder b3 = new EnemyBuilder();
    b3.setXpValue(215);
    Enemy e3 = b3.createEnemy();
    assertEquals("Enemy H:50, S:50, M:50",  e3.toString());
    assertEquals(215, e3.getXpValue());
  }
  
  @Test
  public void testSetAll()
  {
    EnemyBuilder b1 = new EnemyBuilder();
    b1.setName("Bossctopus");
    b1.setVitals(new Vitals(3000, 200, 500));
    b1.setXpValue(2000);
    Enemy e1 = b1.createEnemy();
    assertEquals("Bossctopus H:3000, S:200, M:500", e1.toString());
    assertEquals(2000, e1.getXpValue());
  }
}
